import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by zhangke on 12/01/2018.
 */
public class PileTest extends Observable {
    private ArrayList<Integer> list;
    private int number;
    public int GetNumber(){
        return this.number;
    }

    public ArrayList<Integer> getList() {
        return list;
    }

    public void setList(ArrayList<Integer> list) {
        this.list = list;
    }

    public PileTest (ArrayList<Integer> list) {
        this.list = list ;
    }
    public void push(){
        this.number++;
        setChanged();
        notifyObservers();
    }

    public void pop () {
       this.number--;
        setChanged();
        notifyObservers();
    }
    public void clear(){
        this.list.clear();
    }
}
